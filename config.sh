image_root=http://geni-images.uvalight.net/ben/images

branch=edge
arch=x86_64
mirror=https://skynet.lab.uvalight.net/alpine/${branch}
custom_mirror=https://skynet.lab.uvalight.net/pkg/

kernel_suffix=virthardened
kernelpkg=linux-${kernel_suffix}
kernelimg=vmlinuz-${kernel_suffix}
#kernelimg=vmlinuz
kernel=alpine/${kernelpkg}/boot/${kernelimg}
initrd=build/initrd/initramfs

chroot_dir=build/$vm_image/chroot
fs=build/$vm_image/filesystem
size=1024M
