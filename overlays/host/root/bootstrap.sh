#!/bin/sh

apk add --update docker bridge-utils host-netctl mosquitto-clients
rc-update add docker default
rc-update add host-netctl vnet
