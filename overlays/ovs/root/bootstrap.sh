#!/bin/sh

apk --update add openvswitch

rc-update add ovs-modules vnet
rc-update add ovsdb-server vnet
rc-update add ovs-vswitchd vnet
