#!/usr/bin/env python
from __future__ import print_function
from os import listdir
from sys import stderr
from subprocess import check_call, check_output

ifaces = [f for f in listdir('/sys/class/net') if f not in ('lo', 'eth0')]

if len(ifaces) != 1:
    print('Expected one interface, got', ifaces, file=stderr)
    exit(1)

iface = ifaces[0]

ipv4 = []
for line in check_output(['ip', '-4', 'addr', 'ls', iface]).split('\n'):
    if line.startswith('    inet '):
        ipv4.append(line.split()[1])

if len(ipv4) != 1:
    print('Expected one IPv4 address, got', ipv4, file=stderr)
    exit(1)

gateway = ipv4[0].split('/')[0].split('.')
gateway[-1] = str(int(gateway[-1]) - 1)
gateway = '.'.join(gateway)

# This will fail if the route already exists
check_call(['ip', 'route', 'add', '172.16.0.0/12',
            'via', gateway,
            'dev', ifaces[0]])
