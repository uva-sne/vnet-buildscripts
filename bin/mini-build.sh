#!/bin/bash

VNET_HOME=../vnet

RACK=uva
SLICE=ralph-buildslice-mini
SLICE_CONFIG=slices/buildslice-mini.json

IMG_PUB="http://geni-images.uvalight.net/images/vnet/"
IMG_DST="vnet@geni-images.uvalight.net:vnet-images"

INV=/tmp/build_inv

export PYTHONPATH=${PYTHONPATH}:${VNET_HOME}
export PATH=${VNET_HOME}/bin:${PATH}


orca-request-xml < $SLICE_CONFIG | geni-rpc -u $RACK request $SLICE /dev/stdin
geni-rpc -u $RACK info $SLICE

echo "waiting for slice to boot"

geni-rpc -u $RACK await $SLICE
geni-rpc -u $RACK info $SLICE
geni-rpc -u $RACK hosts $SLICE
rm -rf $INV 
geni-rpc -u $RACK ansible-inv $SLICE > $INV

ansible-playbook -i $INV vnet-switch.yml --limit switch -e "img_pub=$IMG_PUB" -e "img_dest=$IMG_DST" &
ansible-playbook -i $INV vnet-host.yml --limit host -e "img_pub=$IMG_PUB"  -e "img_dest=$IMG_DST" &

wait

geni-rpc  -u $RACK del $SLICE
