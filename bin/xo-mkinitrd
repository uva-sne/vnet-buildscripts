#!/bin/sh

set -e

. ./config.sh

# This script requires a statically linked Busybox to be installed on the host
# system.
static_bb=/bin/busybox

# Kernel package
kernel=alpine/$kernelpkg

# Arguments
chroot_dir=$1
output=$2

xo_kmod_install() {
    mkdir -p "${chroot_dir}/lib/modules/$1"
    if [ -z "$2" ]
    then
        rsync -a "$kernel/lib/modules/$1/" "${chroot_dir}/lib/modules/$1/"
    else
        cp "$kernel/lib/modules/$1/$2" "${chroot_dir}/lib/modules/$1/$2"
    fi
}

kver=$(ls -1 $kernel/lib/modules/ | head -1)

echo "Building initrd for kernel $kver"

rm -fr "$chroot_dir"
mkdir -p $chroot_dir/bin

cp -L alpine/initrd-init $chroot_dir/init
cp -L $static_bb $chroot_dir/bin/busybox
ln -fs busybox $chroot_dir/bin/sh

chmod +x $chroot_dir/init
chmod +x $chroot_dir/bin/busybox

mkdir -p "${chroot_dir}/lib/modules/$kver"
rsync -a "$kernel/lib/modules/$kver/" "${chroot_dir}/lib/modules/$kver/"

(cd $chroot_dir/ && find . -print0 | cpio --null -ov --format=newc) | gzip -7 > "$output"
