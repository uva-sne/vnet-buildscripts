#!/bin/bash

VNET_HOME=../vnet

RACK=uva
SLICE=ralph-buildslice-mini
SLICE_CONFIG=slices/buildslice-mini.json

IMG_PUB="http://geni-images.uvalight.net/images/ralph/"
IMG_DST="ralph@geni-images.uvalight.net:ralph-images"

INV=/tmp/build_inv

export PYTHONPATH=${PYTHONPATH}:${VNET_HOME}
export PATH=${VNET_HOME}/bin:${PATH}


orca-request-xml < $SLICE_CONFIG | geni-rpc -u $RACK request $SLICE /dev/stdin
geni-rpc -u uva info $SLICE 

echo "waiting for slice to boot"

geni-rpc -u uva await $SLICE 
geni-rpc -u uva info $SLICE 
geni-rpc -u uva hosts $SLICE 
rm -rf $INV 
geni-rpc -u uva ansible-inv $SLICE > $INV

ansible-playbook -i $INV vnet-switch.yml --limit switch -e "img_pub=$IMG_PUB" -e "img_dest=$IMG_DST" &
ansible-playbook -i $INV vnet-host.yml --limit host -e "img_pub=$IMG_PUB"  -e "img_dest=$IMG_DST" &

wait

geni-rpc -u uva del $SLICE
