.PHONY: prepare initrd vms deploy
include config.sh

prepare:
	mkdir -p build/cache
	wget ${mirror}/main/${arch}/APKINDEX.tar.gz -O build/cache/APKINDEX.tar.gz
	bin/apk-fetch-latest "${mirror}/main/${arch}/" build/cache apk-tools-static
	#bin/apk-fetch-latest "${mirror}/main/${arch}/" build/cache "${kernelpkg}"

alpine/apk-tools-static: build/cache/apk-tools-static.apk
	rm -rf $@
	mkdir $@
	tar xf $< -C $@ 2>/dev/null

alpine/${kernelpkg}: build/cache/${kernelpkg}.apk
	rm -rf $@
	mkdir $@
	tar xf $< -C $@ 2>/dev/null

# ----

initrd: alpine/apk-tools-static alpine/${kernelpkg}
	sudo bin/xo-mkinitrd "build/initrd/chroot" "build/initrd/initramfs"

#vms: alpine/apk-tools-static alpine/${kernelpkg}
#	./bin/build base bridge host router router:edge nfv

# alpine/${kernelpkg}
vms: alpine/apk-tools-static
	./bin/build-vm host

deploy:
	rsync -av images/ geni-images.uvalight.net:share/images/
	#ssh geni-images.uvalight.net sh deploy.sh
