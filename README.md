# Alpine ExoGENI VM builder

- There is some general configuration such as repo URLs, image size, kernel
  package in `config.sh`.

- Install packages or other tools by modifying the
  `overlays/$vm/root/bootstrap.sh` script. This script will be run in a chroot
  of the VM image. The `base` directory will be used for all VMs.

  You can also modify the `vnet-base` package to provide standard tools and
  dependencies.

- Place files that will be synced over the VM image such as configuration in
  the `overlays/$vm/` directory.


## Commands for qcow2

Run:

    ./bin/build-vm host syslinux

Host is the VM name.


## Commands for ExoGENI

To fetch the latest package manager and kernel package:

    make prepare


To build the initrd:

    sudo make initrd


To build the VMs (builds host image, see makefile):

    make vms


Don't forget to transfer the images from ./images/ to `geni-images.uvalight.net`.


## TODO

- Clean up this repo
- Script to rsync overlays to/from running VMs
